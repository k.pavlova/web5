function CostCalculation() {
    let form = document.forms.form;
    let a = form.elements.quantity.value;
    let b = form.elements.price.value;
    if(a == "" || b == ""){
        alert("Вы не указали количество карандашей или цену");
    } 
    else if(isNaN(a) || isNaN(b) || a<0 || b<0){
        alert("Были введены неправильные значения");
    } 
    else {
        let cost = a * b;
        document.getElementById('cost').innerHTML = "Стоимость карандашей: "+ cost +" р.";    
    }
}
window.addEventListener("DOMContentLoaded", function (event) {
  console.log("DOM fully loaded and parsed");
});